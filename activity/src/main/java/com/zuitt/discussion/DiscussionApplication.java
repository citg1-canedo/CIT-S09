package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication

@RestController
@RequestMapping("/greeting")
public class DiscussionApplication {

	ArrayList<String> enrollees = new ArrayList<>();
	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	@GetMapping("/enroll")
	public String enroll(@RequestParam(value="user", defaultValue = "John")String user){
		enrollees.add(user);
		return String.format("Thankyou for enrolling, %s!", user);
	}

	@GetMapping("/nameage")
	public String nameage(@RequestParam("name")String name, @RequestParam("age")String age) {
		return String.format("Hello %s!, My age is %s.", name,age);
	}

	@GetMapping("/getEnrollees")
	public ArrayList<String> getEnrollees() {
		return enrollees;
	}

	@GetMapping("/course/{id}")
	public String course(@PathVariable("id") String id) {
		String prompt = "";
		switch (id) {
			case "java101":
				prompt = "JAVA 101, MWF 8:00am-11:00am, PHP 3000.00";
				break;
			case "sql101":
				prompt = "SQL 101, TTH 1:00pm-4:00pm, PHP 2000.00";
				break;
			case "javaee101":
				prompt = "JAVA EE 101,MWF 1:00pm-4:00pm, PHP 3500.00";
				break;
			default:
				prompt = "course not found";
				break;
		}
		return prompt;
	}
}
